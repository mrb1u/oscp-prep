---
layout: default
title: Archive
---

  <ul>
    {% for post in site.posts %}
      <li><a href="{{site.baseurl}}{{ post.url }}">{{ post.title }}</a></li>
      {{ post.excerpt }}
    {% endfor %}
  </ul>
